cluster_name     = "test-cluster"
cluster_location = "asia-southeast1-a"

min_master_version = "1.17.17-gke.2800"
node_pools = [
  {
    name            = "pool-1",
    node_count      = 1,
    preemptible     = true,
    machine_type    = "e2-standard-2",
    service_account = "some-service-account-123@gcloud-study-308309.iam.gserviceaccount.com"
  },
  {
    name            = "pool-2",
    node_count      = 1,
    preemptible     = true,
    machine_type    = "e2-standard-2",
    service_account = "some-service-account-123@gcloud-study-308309.iam.gserviceaccount.com"
  }
]

node_pools_tags = {
  all = [
    "all"
  ],
  pool-1 = [
    "pool-1"
  ],
  pool-2 = [
    "pool-2"
  ]
}

node_pools_taints = {
  pool-1 = [
    {
      key    = "gpu",
      value  = "true",
      effect = "NO_SCHEDULE"
    }
  ],
}

