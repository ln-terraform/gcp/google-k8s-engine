locals {
  node_pool_names = [for np in toset(var.node_pools) : np.name]
  node_pools      = zipmap(local.node_pool_names, tolist(toset(var.node_pools)))
}

resource "google_container_cluster" "primary" {
  name               = var.cluster_name
  location           = var.cluster_location
  node_locations     = var.node_locations
  network            = var.network
  subnetwork         = var.subnetwork
  min_master_version = var.min_master_version

  initial_node_count = 1
  # We can't create a cluster with no node pool defined, but we want to only use
  # separately managed node pools. So we create the smallest possible default
  # node pool and immediately delete it.
  remove_default_node_pool = true

  maintenance_policy {
    daily_maintenance_window {
      start_time = var.maintenance_policy.start_time
    }
  }

  ip_allocation_policy {
    cluster_secondary_range_name  = lookup(var.ip_allocation_policy, "cluster_secondary_range_name", null)
    services_secondary_range_name = lookup(var.ip_allocation_policy, "services_secondary_range_name", null)
  }
}

resource "google_container_node_pool" "pools" {
  for_each = local.node_pools
  name     = each.value["name"]
  location = var.cluster_location

  cluster           = google_container_cluster.primary.name
  node_count        = each.value["node_count"]
  max_pods_per_node = try(each.value["max_pods_per_node"], 110)

  node_config {
    preemptible  = each.value["preemptible"]
    machine_type = each.value["machine_type"]
    image_type   = var.image_type
    # Google recommends custom service accounts that have cloud-platform scope and permissions granted via IAM Roles.
    service_account = each.value["service_account"]

    tags = concat(
      lookup(var.node_pools_tags, "all", []),
      lookup(var.node_pools_tags, each.value["name"], [])
    )

    dynamic "taint" {
      for_each = lookup(var.node_pools_taints, each.value["name"], [])
      content {
        key    = taint.value["key"]
        value  = taint.value["value"]
        effect = taint.value["effect"]
      }
    }

    oauth_scopes = [
      "https://www.googleapis.com/auth/cloud-platform"
    ]
  }

  management {
    auto_repair  = var.node_management.auto_repair
    auto_upgrade = var.node_management.auto_upgrade
  }
}
