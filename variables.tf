variable "cluster_name" {
  type = string
}

variable "cluster_location" {
  type = string
}

variable "node_locations" {
  type    = list(string)
  default = []
}

variable "network" {
  type    = string
  default = null
}

variable "subnetwork" {
  type    = string
  default = null
}

variable "node_pools" {
  type = list(map(string))
}

variable "min_master_version" {
  type    = string
  default = null
}

variable "image_type" {
  type    = string
  default = null
}

variable "node_pools_tags" {
  type = map(list(string))
  default = {
    all = []
  }
}

variable "maintenance_policy" {
  type = map(string)
  default = {
    start_time = "18:00"
  }
}

variable "node_management" {
  type = map(string)
  default = {
    auto_repair  = false
    auto_upgrade = false
  }
}

variable "ip_allocation_policy" {
  type    = map(string)
  default = {}
}

variable "node_pools_taints" {
  type    = map(list(map(string)))
  default = {}
}